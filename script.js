$(document).ready(function() {   // for image preview
        $("#fileUpload").on('change', function() {
          //Get count of selected files
          var countFiles = $(this)[0].files.length;
          var imgPath = $(this)[0].value;
          var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
          var image_holder = $("#image-holder");
          image_holder.empty();
          if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
            if (typeof(FileReader) != "undefined") {
              //loop for each file selected for uploaded.
              for (var i = 0; i < countFiles; i++)
              {
                var reader = new FileReader();
                reader.onload = function(e) {
                  $("<img />", {
                    "src": e.target.result,
                    "class": "thumb-image"
                  }).appendTo(image_holder);
                }
                image_holder.show();
                reader.readAsDataURL($(this)[0].files[i]);
              }
            } else {
              alert("This browser does not support FileReader.");
            }
          } else {
            alert("Pls select only images");
          }
        });
});


$(function(){              // for form validation
      $("form[name='regestration']").validate({
        rules: {
          name: {
            required: true,

          },
          email: {
            required: true,
            email: true
          },
          password: {
            required: true,
            minlength: 5
          },
          cpassword: {
            required: true,
            minlength: 5,
            equalTo: '#password'
          },
          image:
          {
            required:true,
            accept: "image/*"
          }
        },

          submitHandler: function(form){
          var filepath = $('#image').val();
          var results  = $('#display');
          var formdata = new FormData($('#signup')[0]);
          formdata.append('filename', $('input[type=file]')[0].files[0]);

          $.ajax({
              type: 'POST',
              url: 'regdata.php',
              data: formdata,
              cache: false,
              contentType: false,
              processData: false,
              success: function(data)
              {
                $('#signup')[0].reset();
                results.html(data);
              }
            });
          return false;
          }
      });
});
